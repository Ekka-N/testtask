import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import usersStore from '../models/UsersStore';

interface EditUserProps {
    match: any;
}

const EditUser: React.FC<EditUserProps> = ({ match }) => {
    const ID = match.params.userID;

    const users = usersStore.usersState;

    const [firstName, setFirstName] = useState(users[ID].name.first);
    const [lastName, setLastName] = useState(users[ID].name.last);
    const [age, setAge] = useState(users[ID].age);

    const handleChangeFirstName = (e: any) => {
        setFirstName(e.target.value);
    };
    const handleChangeLastName = (e: any) => {
        setLastName(e.target.value);
    };
    const handleChangeAge = (e: any) => {
        setAge(e.target.value);
    };
    const handleClick = () => {
        const editedObj = {
            guid: users[ID].guid,
            name: { first: firstName, last: lastName },
            age: age,
        };

        usersStore.editUserItem(editedObj);
    };

    return (
        <div className="edituser row">
            <div className="col-md-6 col-sm-12 col-lg-6 col-md-offset-3">
                <div className="panel panel-default">
                    <div className="panel-body">
                        <form name="myform">
                            <div className="form-group">
                                <label htmlFor="myName">First Name *</label>
                                <input
                                    id="name_first"
                                    name="name_first"
                                    className="form-control"
                                    type="text"
                                    onChange={handleChangeFirstName}
                                    value={firstName}
                                    data-validation="required"
                                />
                                <span id="error_name" className="text-danger"></span>
                            </div>
                            <div className="form-group">
                                <label htmlFor="lastname">Last Name *</label>
                                <input
                                    id="name_last"
                                    name="name_last"
                                    className="form-control"
                                    onChange={handleChangeLastName}
                                    value={lastName}
                                    type="text"
                                    data-validation="email"
                                />
                                <span id="error_lastname" className="text-danger"></span>
                            </div>
                            <div className="form-group">
                                <label htmlFor="age">Age *</label>
                                <input
                                    id="age"
                                    name="age"
                                    className="form-control"
                                    onChange={handleChangeAge}
                                    value={age}
                                    type="number"
                                    min="1"
                                />
                                <span id="error_age" className="text-danger"></span>
                            </div>
                            <Link to="/">
                                <Button
                                    variant="outline-success"
                                    id="submit"
                                    onClick={handleClick}
                                >
                                    Сохранить
                                </Button>
                            </Link>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EditUser;
