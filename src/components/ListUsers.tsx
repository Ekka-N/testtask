import React, { useState, useEffect } from 'react';
import { observer } from 'mobx-react';
import usersStore from '../models/UsersStore';
import { Link } from 'react-router-dom';
import UserRowItem from './UserRowItem';
import Button from 'react-bootstrap/Button';

const ListUsers: React.FC = () => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    // const [sortOrder, setSortOrder] = useState(1);
    const users = usersStore.usersState;

    const [sortedList, setSortedList] = useState(users);

    const itemsList = sortedList.slice().reduce((acc: any, value: any, index: any) => {
        const userIndex = usersStore.usersState.slice().findIndex((el: any) => {
            return el.name.first === value.name.first && el.name.last === value.name.last;
        });

        return userIndex === -1
            ? acc
            : acc.concat([
                  <UserRowItem
                      key={`userItem_${index}`}
                      value={value}
                      userID={userIndex}
                  />,
              ]);
    }, []);

    // Здесь нужно реализовать функцию сортировки в таблице по заголовкам
    const sort = (e: any) => {
        let newArray = [...sortedList];

        if (newArray !== []) {
            newArray = newArray.sort((a, b) => {
                switch (e.target.id) {
                    case 'sort-first-name':
                        return a === b ? 0 : a.name.first < b.name.first ? -1 : 1;
                    case 'sort-last-name':
                        return a === b ? 0 : a.name.last < b.name.last ? -1 : 1;
                    case 'sort-age':
                        return a === b ? 0 : a.age > b.age ? -1 : 1;
                    default:
                        return 0;
                }
            });
        }

        if (e.target.id === 'sort-index') setSortedList(users);
        else setSortedList(newArray);
    };

    // Здесь нужно реализовать функцию фильтрации в таблице производя, используйте инпут
    useEffect(() => {
        if (firstName) {
            let newArray = [...sortedList];

            newArray = newArray.filter(
                obj => obj.name.first.toLowerCase().search(firstName.toLowerCase()) >= 0,
            );
            setSortedList(newArray);
        } else setSortedList(users);
    }, [firstName]);

    useEffect(() => {
        if (lastName) {
            let newArray = [...sortedList];

            newArray = newArray.filter(
                obj => obj.name.last.toLowerCase().search(lastName.toLowerCase()) >= 0,
            );
            setSortedList(newArray);
        } else setSortedList(users);
    }, [lastName]);

    useEffect(() => {
        if (age) {
            let newArray = [...users];

            newArray = newArray.filter(obj => obj.age == age);
            setSortedList(newArray);
        } else setSortedList(users);
    }, [age]);

    return (
        <div className="userListDiv">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>
                            <Button variant="outline-dark" id="sort-index" onClick={sort}>
                                #
                            </Button>
                        </th>
                        <th>
                            <Button
                                variant="outline-dark"
                                id="sort-first-name"
                                onClick={sort}
                            >
                                First Name
                            </Button>
                        </th>
                        <th>
                            <Button
                                variant="outline-dark"
                                id="sort-last-name"
                                onClick={sort}
                            >
                                Last Name
                            </Button>
                        </th>
                        <th>
                            <Button variant="outline-dark" id="sort-age" onClick={sort}>
                                Age
                            </Button>
                        </th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr key="tr_filter">
                        <td key="id_filter">-</td>
                        <td key="nf_input">
                            <input
                                type="text"
                                name="firstName"
                                value={firstName}
                                onChange={e => setFirstName(e.target.value)}
                            ></input>
                        </td>
                        <td key="nl_input">
                            <input
                                type="text"
                                name="lastName"
                                value={lastName}
                                onChange={e => setLastName(e.target.value)}
                            ></input>
                        </td>
                        <td key="age_input">
                            <input
                                type="number"
                                min="1"
                                step="1"
                                name="age"
                                value={age}
                                onChange={e => setAge(e.target.value)}
                            ></input>
                        </td>
                        <td key="actoions_input">
                            <span></span>
                            <span></span>
                        </td>
                    </tr>
                    {itemsList}
                </tbody>
            </table>
            <Link to="/create">
                <Button variant="outline-dark">Создать</Button>
            </Link>
        </div>
    );
};

export default observer(ListUsers);
