import React from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import usersStore from '../models/UsersStore';

type userType = {
    guid: string;
    age: number;
    name: any;
    email: string;
};

interface UserRowItemProps {
    value: userType;
    userID: string;
}
const UserRowItem: React.FC<UserRowItemProps> = ({ value, userID }) => {
    const userIDtoUse = parseInt(userID, 10);
    const { guid, age, name } = value;

    return (
        <tr key={userIDtoUse}>
            <td key={`id_${guid}`}>{userIDtoUse + 1}</td>
            <td key={`nf_${guid}`}>{name.first}</td>
            <td key={`nl_${guid}`}>{name.last}</td>
            <td key={`age_${guid}`}>{age}</td>
            <td key={`actoions_${guid}`}>
                <span key={`actedit_${guid}`} className="actions">
                    <Link key={`lnk_ed_${guid}`} to={`/edit/${userIDtoUse}`}>
                        <Button variant="outline-secondary">Edit</Button>
                    </Link>
                </span>
                <span key={`actdelete_${guid}`} className="actions">
                    <Link to="/" onClick={() => usersStore.deleteUserItem(guid)}>
                        <Button variant="outline-danger">Del</Button>
                    </Link>
                </span>
            </td>
        </tr>
    );
};
export default UserRowItem;
