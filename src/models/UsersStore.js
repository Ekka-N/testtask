import { action, extendObservable } from 'mobx';
import { v4 as uuidv4 } from 'uuid';
import usersDB from '../users.json';

class ObservableUsersStore {
    constructor() {
        extendObservable(this, {
            users: [],
            get usersState() {
                return this.users;
            },
        });
    }

    saveLocal() {
        const lst = typeof localStorage === 'undefined' ? {} : localStorage;

        lst.users = JSON.stringify(this.usersState);
    }
    // Пример функции добавления
    addUserItem = action('addUserItem', function(userItem) {
        const guid = uuidv4();

        this.users.push({ ...userItem, ...{ guid } });
        this.saveLocal();
    });
    // Добавить функцию удаления
    deleteUserItem = action('deleteUserItem', function(guid) {
        this.users = this.users.filter(item => item.guid !== guid);
        this.saveLocal();
        window.location.reload();
    });

    // Добавить функцию редактирования
    editUserItem = action('editUserItem', function(userItemEdited) {
        this.users = this.users.map(userItem => {
            if (userItemEdited.guid === userItem.guid) {
                userItem.name.first = userItemEdited.name.first;
                userItem.name.last = userItemEdited.name.last;
                userItem.age = userItemEdited.age;
            }

            return userItem;
        });
        this.saveLocal();
    });
}

const insertData = store => {
    const lst = typeof localStorage === 'undefined' ? {} : localStorage;

    if (!!lst.users && JSON.parse(lst.users).length > 0) {
        JSON.parse(lst.users).forEach(e => store.addUserItem(e));
    } else {
        usersDB.forEach(e => store.addUserItem(e));
    }

    return store;
};

const usersStore = insertData(new ObservableUsersStore());

export default usersStore;
